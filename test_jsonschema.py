from jsonschema import validate, __version__, ValidationError


class TestJsonSchema:
    package = 'jsonschema'
    version = __version__

    def __init__(self, allow_extra):
        schema = {
            "title": "Model",
            "type": "object",
            "properties": {
                "id": {
                    "title": "Id",
                    "type": "integer"
                },
                "client_name": {
                    "title": "Client Name",
                    "maxLength": 255,
                    "type": "string"
                },
                "sort_index": {
                    "title": "Sort Index",
                    "type": "number"
                },
                "client_phone": {
                    "title": "Client Phone",
                    "maxLength": 255,
                    "type": "string"
                },
                "location": {
                    "$ref": "#/definitions/Location"
                },
                "upstream_http_referrer": {
                    "title": "Upstream Http Referrer",
                    "maxLength": 1023,
                    "type": "string"
                },
                "grecaptcha_response": {
                    "title": "Grecaptcha Response",
                    "minLength": 20,
                    "maxLength": 1000,
                    "type": "string"
                },
                "last_updated": {
                    "title": "Last Updated",
                    "type": "string",
                    "format": "date-time"
                },
                "skills": {
                    "title": "Skills",
                    "default": [],
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/Skill"
                    }
                }
            },
            "required": [
                "id",
                "client_name",
                "sort_index",
                "grecaptcha_response"
            ],
            "definitions": {
                "Location": {
                    "title": "Location",
                    "type": "object",
                    "properties": {
                        "latitude": {
                            "title": "Latitude",
                            "type": "number"
                        },
                        "longitude": {
                            "title": "Longitude",
                            "type": "number"
                        }
                    }
                },
                "Skill": {
                    "title": "Skill",
                    "type": "object",
                    "properties": {
                        "subject": {
                            "title": "Subject",
                            "type": "string"
                        },
                        "subject_id": {
                            "title": "Subject Id",
                            "type": "integer"
                        },
                        "category": {
                            "title": "Category",
                            "type": "string"
                        },
                        "qual_level": {
                            "title": "Qual Level",
                            "type": "string"
                        },
                        "qual_level_id": {
                            "title": "Qual Level Id",
                            "type": "integer"
                        },
                        "qual_level_ranking": {
                            "title": "Qual Level Ranking",
                            "default": 0,
                            "type": "number"
                        }
                    },
                    "required": [
                        "subject",
                        "subject_id",
                        "category",
                        "qual_level",
                        "qual_level_id"
                    ]
                }
            }
        }

        self.schema = schema

    def validate(self, data):
        try:
            return True, validate(data, self.schema)
        except ValidationError as e:
            return False, e.message

    def to_json(self, model):
        return model.schema

To set up: `pip install -r requirements.txt`

To run: `python run.py`

JSON schema generation to match the Model in `test_pydantic`: I manually grabbed the output of `json.dumps(self.model.schema())` for a run.

Sample run results for jsonschema
```
testing pydantic, jsonschema, 5 times each
      pydantic (1/5) time=0.316s, success=51.35%
      pydantic (2/5) time=0.325s, success=51.35%
      pydantic (3/5) time=0.345s, success=51.35%
      pydantic (4/5) time=0.412s, success=51.35%
      pydantic (5/5) time=0.332s, success=51.35%
      pydantic best=0.316s, avg=0.346s, stdev=0.038s

    jsonschema (1/5) time=8.984s, success=37.85%
    jsonschema (2/5) time=8.984s, success=37.85%
    jsonschema (3/5) time=9.019s, success=37.85%
    jsonschema (4/5) time=8.988s, success=37.85%
    jsonschema (5/5) time=9.423s, success=37.85%
    jsonschema best=8.984s, avg=9.080s, stdev=0.193s

      pydantic best=52.678μs/iter avg=57.628μs/iter stdev=6.390μs/iter version=1.6.1
    jsonschema best=1497.382μs/iter avg=1513.281μs/iter stdev=32.119μs/iter version=3.2.0
```